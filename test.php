<?php

$config = array(
    'smtp_username' => 'noreply',          //Смените на имя своего почтового ящика.
    'smtp_port'     => '25',               // Порт работы. Не меняйте, если не уверены.
    'smtp_host'     => 'mail.example.com', //сервер для отправки почты(для наших клиентов менять не требуется)
    'smtp_password' => 'passwd',           //Измените пароль
    'smtp_debug'    => true,               //Если Вы хотите видеть сообщения ошибок, укажите true вместо false
    'smtp_charset'  => 'Windows-1251',     //кодировка сообщений. (или UTF-8, итд)
    'smtp_from'     => 'Уведомление ВМП'   //Ваше имя - или имя Вашего сайта. Будет показывать при прочтении в поле "От кого"
);

/**
 * @param Talon $tn
 */
function talon_stage2_no_acc_save($tn){

  global $config;
  
  if($tn->talonFields['TALON_NAPRAVLEN']['ACTIVATED_BY_MU'] == 0){
      if($tn->oms == 1 && $tn->existOtherTalonsOms){
          echo writeMessage('Пациент уже на лечении по 1 разделу<br />');
          return false;
      }

      if($tn->oms == 0 && $tn->existOtherTalonsNotOms){
          echo writeMessage('Пациент уже на лечении по 2 разделу<br />');
          return false;
      }

  }

  $card = new PatientCard($tn->id_patient);
  $card->getDataVmpStage0($tn->id_talon);
  $messageHelper = new MessageHelper();
  
  $id_stage = isset($tn->talonFields['VMP_STAGE2']['ID'])?$tn->talonFields['VMP_STAGE2']['ID']:'';
  $talon_date = $tn->year_talon;    
  $talon_date_arr = array(date('Y'));
  if(USER::iCan('talons_old'))
      array_push ($talon_date_arr, date('Y')-1);
  
  $statusOtkazDecis = GetMainQuery::getStatusOtkazDecis($_REQUEST['id_decis_code']);//получаем признак, показывающий отказ пациенту
  $data['id_talon_napr']['value'] = $tn->id_talon;
  $data['id_talon_napr']['required'] = 1;
  if(USER::myRole('vmp_mu') && $id_stage && in_array($talon_date, $talon_date_arr) && $tn->talonFields['TALON_NAPRAVLEN']['MAX_ETAP'] > 2 && $tn->is_enabled && !$tn->otkaz)
  {
    $data['plan_date_hosp']['value'] = (isset($_REQUEST['plan_date_hosp']) ? "TO_DATE('".Service::stripText($_REQUEST['plan_date_hosp'])."','dd.mm.yyyy')" : '');
    $data['plan_date_hosp']['required'] = 1;
    $data['id_vmp_vid']['value'] = (isset($_REQUEST['id_vmp_vid']) ? Service::stripNumber($_REQUEST['id_vmp_vid']) : '');
    $data['id_vmp_vid']['required'] = 1;
    $data['fio_resp_face']['value'] = (isset($_REQUEST['fio_resp_face']) ? "'".Service::stripText($_REQUEST['fio_resp_face'],100)."'" : '');
    $data['fio_resp_face']['required'] = 1;
    $data['position_name']['value'] = (isset($_REQUEST['position_name']) ? "'".Service::stripText($_REQUEST['position_name'],100)."'" : '');
    $data['position_name']['required'] = 1;
    $data['resp_phone']['value'] = (isset($_REQUEST['resp_phone']) ? "'".Service::stripText($_REQUEST['resp_phone'],100)."'" : '');
    $data['resp_phone']['required'] = 1;
    $data['resp_email']['value'] = (isset($_REQUEST['resp_email']) ? "'".Service::stripText($_REQUEST['resp_email'],100)."'" : '');
    $data['resp_email']['required'] = 1;
    $data['last_user_id']['value'] = user::$id;
    $data['last_user_id']['required'] = 1;
  }
  else
  {
    $data['type_notice']['value'] = (isset($_REQUEST['type_notice']) ? Service::stripNumber($_REQUEST['type_notice']) : '');
    $data['type_notice']['required'] = 1;
    $data['id_decis_code']['value'] = (isset($_REQUEST['id_decis_code']) ? Service::stripNumber($_REQUEST['id_decis_code']) : '');
    $data['id_decis_code']['required'] = 1;
    $data['decis_date']['value'] = (isset($_REQUEST['decis_date']) ? "TO_DATE('".Service::stripText($_REQUEST['decis_date'])."','dd.mm.yyyy')" : '');
    $data['decis_date']['required'] = 1;
    $data['date_registration_docs']['value'] = (isset($_REQUEST['date_registration_docs']) ? "TO_DATE('".Service::stripText($_REQUEST['date_registration_docs'])."','dd.mm.yyyy')" : '');
    $data['date_registration_docs']['required'] = 1;
    $data['id_vmp_vid']['value'] = (isset($_REQUEST['id_vmp_vid']) ? Service::stripNumber($_REQUEST['id_vmp_vid']) : '');
    $data['id_vmp_vid']['required'] = 1;
    $data['plan_date_hosp']['value'] = (isset($_REQUEST['plan_date_hosp']) ? "TO_DATE('".Service::stripText($_REQUEST['plan_date_hosp'])."','dd.mm.yyyy')" : '');
    $data['plan_date_hosp']['required'] = 1;
    $data['date_send_to_ouz']['value'] = (isset($_REQUEST['date_send_to_ouz']) ? "TO_DATE('".Service::stripText($_REQUEST['date_send_to_ouz'])."','dd.mm.yyyy')" : '');
    $data['date_send_to_ouz']['required'] = 1;
    $data['date_doc_from_ouz']['value'] = (isset($_REQUEST['date_doc_from_ouz']) ? "TO_DATE('".Service::stripText($_REQUEST['date_doc_from_ouz'])."','dd.mm.yyyy')" : '');
    $data['date_doc_from_ouz']['required'] = 1;
    $data['date_ots_gosp']['value'] = (isset($_REQUEST['date_ots_gosp']) ? "TO_DATE('".Service::stripText($_REQUEST['date_ots_gosp'])."','dd.mm.yyyy')" : '');
    $data['date_ots_gosp']['required'] = 0;
    $data['fio_resp_face']['value'] = (isset($_REQUEST['fio_resp_face']) ? "'".Service::stripText($_REQUEST['fio_resp_face'],100)."'" : '');
    $data['fio_resp_face']['required'] = 1;
    $data['position_name']['value'] = (isset($_REQUEST['position_name']) ? "'".Service::stripText($_REQUEST['position_name'],100)."'" : '');
    $data['position_name']['required'] = 1;
    $data['resp_phone']['value'] = (isset($_REQUEST['resp_phone']) ? "'".Service::stripText($_REQUEST['resp_phone'],100)."'" : '');
    $data['resp_phone']['required'] = 1;
    $data['resp_email']['value'] = (isset($_REQUEST['resp_email']) ? "'".Service::stripText($_REQUEST['resp_email'],100)."'" : '');
    $data['resp_email']['required'] = 1;
    $data['sogl_date_hosp']['value'] = (isset($_REQUEST['sogl_date_hosp']) ? 1 : 0);
    $data['sogl_date_hosp']['required'] = 0;
    $data['prior_decis']['value'] = 'null';
    $data['prior_decis']['required'] = 1;
    $data['last_user_id']['value'] = user::$id;
    $data['last_user_id']['required'] = 1;
  }
  $null_values = false;
  foreach ($data as $key => $val)
  {
    if ($val['required'] && $val['value'] == '')
      $null_values = true;
    if (!$val['required'] && $val['value'] == '')
      $data[$key]['value'] = 'null';
  }
  if (!$null_values)
  {
    if(!$statusOtkazDecis){
      $profile = $tn->talonFields['VMP_STAGE1']['ID_MH_PROFILE'];
      $id_med_inst = $tn->talonFields['VMP_STAGE1']['ID_MED_INST'];
      $id_group = $tn->talonFields['VMP_STAGE1']['ID_GROUP']?$tn->talonFields['VMP_STAGE1']['ID_GROUP']:0;
      $result = GetMainQuery::plnLech($profile, $id_med_inst, $tn->year_talon, $id_group, $tn->id_finance_source);
      $rez_plan = Service::stripNumber($result['PLN']);
      $rez_lech = Service::stripNumber($result['LECHEN']);
    }
      
  if(isset($tn->talonFields['VMP_STAGE2']['ID'])&&$tn->talonFields['VMP_STAGE2']['ID']){
      if ($statusOtkazDecis || !$tn->controlQuotaTalon() || ($rez_plan>$rez_lech && $tn->list_wait) || ($rez_plan>=$rez_lech && !$tn->list_wait && !$tn->otkaz) || ($rez_plan>$rez_lech && !$tn->list_wait && $tn->otkaz))
      {
        /* Проверка на изменения поля дата планируемой госпитализации */
        if(Service::stripText($_REQUEST['plan_date_hosp'])!=$tn->talonFields['VMP_STAGE2']['PLAN_DATE_HOSP']){
            /* Отправляем уведомление пациенту */
            $headers_ = "MIME-Version: 1.0\r\n";
            $headers_ .= "Content-type: text/html; charset=".$config['smtp_charset']."\r\n";
            $headers_ .= "To: ".$tn->talonFields['VMP_STAGE2']['EMAIL']."\r\n";
            $headers_ .= "From: ".$config['smtp_from']." <".$config['smtp_username']."@".$config['smtp_host'].">";
            $subject_ = 'Уведомление о новой дате планируемой госпитализации';
            $message_ = 'Добрый день. Уведомляем Вас о новой дате госпитализации - '.Service::stripText($_REQUEST['plan_date_hosp']);
            $smpt = new SMPT($config['smtp_host'],$config['smtp_port'],$config['smtp_username'],$config['smtp_password']);
            if(!empty($tn->talonFields['VMP_STAGE2']['EMAIL']))
                $smpt->send($tn->talonFields['VMP_STAGE2']['EMAIL'],$subject_,$message_,$headers_);
            if(!empty($tn->talonFields['VMP_STAGE2']['EMAIL']))
                $smpt->smtpmail($tn->talonFields['VMP_STAGE2']['EMAIL'],$subject_,$message_,$config);
            
            /* Уведомление пользователю ОУЗ и МО */
            $prof_descr = ' (Профиль: '.str_replace('.','',$tn->talonFields['VMP_STAGE1']['PROFILE_CODE']).' - '.ucfirst($tn->talonFields['VMP_STAGE1']['PROFILE_DESCR']).')';
            $entry_title = 'Изменение даты госпитализации у пациента. Номер талона: '.$tn->uniq_number;
            $entry_body = 'Пациенту с номером талона '.$tn->uniq_number.$prof_descr.' перенесли дату госпитализации.<br/><br/>';
            $entry_body .= '<span class="link-act" onclick="goToEtap(2,'.$tn->id_talon.');">Перейти к '.($tn->itsMO()?'просмотру':'заполнению 2 Этапа').'</span><br/><br/>';
            $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
            $entry_body .= $card->fieldsPatient->fio.'</span><br/>';
            $entry_body .= 'Субъект РФ: '.$tn->patient_terr_name.'<br/>';
            $entry_body .= 'МО: '.$tn->talonFields['VMP_STAGE1']['INST_NAME'].'<br/>';
            //if($tn->itsMO()){
              $toUsers1 = getUsers('MU','',$tn->talonFields['VMP_STAGE1']['ID_MED_INST']);
            //}else{
              $toUsers2 = getUsers('OUZ',$tn->ouz_terr);
            //}
            $messageHelper->sendMessages(4, USER::$id, $toUsers1, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
        }
        $values = '';
        foreach ($data as $key=>$val)
        {
          $values .= $key.' = '.$val['value'].', ';
        }
        $values[strlen($values)-2] = ' ';

        $sql = "UPDATE vmp_stage2 SET ".$values." WHERE id = " . intval($tn->talonFields['VMP_STAGE2']['ID']) ;
      }
      else
      {
        $sql = '';
        echo writeMessage('Закончились объемы по выбранному профилю<br />');
      }
    
  }else{
      if ($statusOtkazDecis || !$tn->controlQuotaTalon() || ($rez_plan>$rez_lech && $tn->list_wait) || ($rez_plan>=$rez_lech && !$tn->list_wait && !$tn->otkaz) || ($rez_plan>$rez_lech && !$tn->list_wait && $tn->otkaz))
      {
        $values1 = '(';
        $values2 = '(';
        foreach ($data as $key=>$val)
        {
          $values1 .= $key.', ';
          $values2 .= $val['value'].', ';
        }
        $values1[strlen($values1)-2] = ')';
        $values2[strlen($values2)-2] = ')';
        $sql = "insert into vmp_stage2 ".$values1."
            values ".$values2;
      }
      else
      {
        $sql = '';
        echo writeMessage('Закончились объемы по выбранному профилю<br />');
      }
    }
    $rs = new Recordset('mz_registr_ht');
    if($sql!=''){
      $success = $rs->execute($sql);
    }
    if ($success && $sql != '')
    {
      if($tn->itsMO()){
        $bindsSt3 = array(
          ':ID_TALON_NAPR' => $tn->id_talon,
          ':FIO_RESP_FACE' => isset($_REQUEST['fio_resp_face'])?Service::stripText($_REQUEST['fio_resp_face'],100):' ',
          ':POSITION_NAME' => isset($_REQUEST['position_name'])?$_REQUEST['position_name']:' ',
          ':LAST_USER_ID' => user::$id
        );
        $sqlInsStage3 = "
          INSERT INTO vmp_stage3(id_talon_napr, fio_resp_face, position_name,last_user_id)
          VALUES( :ID_TALON_NAPR , :FIO_RESP_FACE , :POSITION_NAME , :LAST_USER_ID )
        ";
        $rs->execute($sqlInsStage3, array(), $bindsSt3);
      }

      if($tn->max_etap >=3 && ($tn->id_finance_source == 3 || $tn->id_finance_source == 5)){
          $bindsSt3 = array(
              ':ID_TALON_NAPR' => $tn->id_talon,
              ':FIO_RESP_FACE' => isset($_REQUEST['fio_resp_face'])?Service::stripText($_REQUEST['fio_resp_face'],100):' ',
              ':POSITION_NAME' => isset($_REQUEST['position_name'])?$_REQUEST['position_name']:' ',
              ':LAST_USER_ID' => user::$id
          );
          $sqlInsStage3 = "update vmp_stage3 set fio_resp_face = :FIO_RESP_FACE , 
                                                 position_name = :POSITION_NAME , 
                                                 last_user_id = :LAST_USER_ID 
                           where id_talon_napr = :ID_TALON_NAPR";
          $rs->execute($sqlInsStage3, array(), $bindsSt3);
      }
      
      if($_REQUEST['id_decis_code'] == 9){//отправка на дообследование
          /* Отправляем уведомление пациенту */
          if(!empty($tn->talonFields['VMP_STAGE2']['EMAIL'])) {
              $headers_ = "MIME-Version: 1.0\r\n";
              $headers_ .= "Content-type: text/html; charset=".$config['smtp_charset']."\r\n";
              $headers_ .= "To: ".$tn->talonFields['VMP_STAGE2']['EMAIL']."\r\n";
              $headers_ .= "From: ".$config['smtp_from']." <".$config['smtp_username']."@".$config['smtp_host'].">";
              $subject_ = 'Уведомление о необходимости дообследования';
              $message_ = 'Добрый день. Уведомляем Вас о необходимости дообследования перед проведением госпитализации';
              $smpt = new SMPT($config['smtp_host'],$config['smtp_port'],$config['smtp_username'],$config['smtp_password']);
              $smpt->send($tn->talonFields['VMP_STAGE2']['EMAIL'],$subject_,$message_,$headers_);
              $smpt->smtpmail($tn->talonFields['VMP_STAGE2']['EMAIL'],$subject_,$message_,$config);
          }

                                                $sql = "UPDATE vmp_stage1 SET is_returned = 1, last_user_id = ".user::$id." WHERE id_talon_napr = $tn->id_talon ";
                                                $rs->execute($sql,array($tn->id_talon));

                                                $prof_descr = ' (Профиль: '.str_replace('.','',$tn->talonFields['VMP_STAGE1']['PROFILE_CODE']).' - '.ucfirst($tn->talonFields['VMP_STAGE1']['PROFILE_DESCR']).')';
                                                $entry_title = 'Талон №'.$tn->uniq_number.' возвращен на первый этап';
                                                $entry_body .= SandBox::$titleMU.' уведомляет Вас о том, что талон №'.$tn->uniq_number.$prof_descr.' возвращен на 1 Этап.<br/><br/>';
                                                $entry_body .= '<span class="link-act" onclick="goToEtap(1,'.$tn->id_talon.');">Перейти к заполнению 1 Этапа</span><br/><br/>';
                                                $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
                                                $entry_body .= $card->fieldsPatient->fio.'</span><br/>';
                                                $entry_body .= 'Субъект РФ: '.$tn->patient_terr_name.'<br/>';
                                                $entry_body .= 'МО: '.$tn->talonFields['VMP_STAGE1']['INST_NAME'].'<br/>';
            if($tn->itsMO()){
              $toUsers = getUsers('MU','',$tn->id_medinst);
            }else{
              $toUsers = getUsers('OUZ',$tn->ouz_terr);
            }
            $messageHelper->sendMessages(4, USER::$id, $toUsers, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
      }else{
        //нам не нужно отправлять уведомления если талон был проапдетен с правами ВСЕ МОГУ!
        if(!(isset($tn->talonFields['VMP_STAGE2']['ID'])&&$tn->talonFields['VMP_STAGE2']['ID'] && $tn->getICanSaveAllStage())){
          if($statusOtkazDecis){
            $prof_descr = ' (Профиль: '.str_replace('.','',$tn->talonFields['VMP_STAGE1']['PROFILE_CODE']).' - '.ucfirst($tn->talonFields['VMP_STAGE1']['PROFILE_DESCR']).')';
            $entry_title = 'Направленному Вами пациенту отказано в оказании ВМП. Номер талона: '.$tn->uniq_number;
            $entry_body .= 'Пациенту с номером талона '.$tn->uniq_number.$prof_descr.' отказано в оказании ВМП.<br/><br/>';
            $entry_body .= '<span class="link-act" onclick="goToEtap(6,'.$tn->id_talon.');">Перейти к '.($tn->itsMO()?'просмотру':'заполнению 6 Этапа').'</span><br/><br/>';
            $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
            $entry_body .= $card->fieldsPatient->fio.'</span><br/>';
            $entry_body .= 'Субъект РФ: '.$tn->patient_terr_name.'<br/>';
            $entry_body .= 'МО: '.$tn->talonFields['VMP_STAGE1']['INST_NAME'].'<br/>';
            if($tn->itsMO()){
              $toUsers = getUsers('MU','',$tn->id_medinst);
            }else{
              $toUsers = getUsers('OUZ',$tn->ouz_terr);
            }
            $messageHelper->sendMessages(4, USER::$id, $toUsers, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
          }else{
            $entry_body = '';
            if (strpos(strtolower($sql),'insert') !== false)
            {
              $entry_title = 'Талон №'.$tn->uniq_number.' переведен на третий этап';
              $prof_descr = ' (Профиль: '.str_replace('.','',(isset($tn->talonFields['VMP_STAGE1']['PROFILE_CODE'])?$tn->talonFields['VMP_STAGE1']['PROFILE_CODE']:'')).' - '.ucfirst(isset($tn->talonFields['VMP_STAGE1']['PROFILE_DESCR'])?$tn->talonFields['VMP_STAGE1']['PROFILE_DESCR']:'').')';
              $entry_body .= SandBox::$titleMU.' уведомляет Вас о том, что талон №'.$tn->uniq_number.$prof_descr.' переведен на 3 Этап.<br/>';
              $entry_body .= '<br/>';
              $entry_body .= '<span class="link-act" onclick="goToEtap(3,'.$tn->id_talon.');">Перейти к '.($tn->itsMO()?'просмотру':'заполнению 3 этапа').'</span><br/><br/>';
              $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
              $entry_body .= $card->fieldsPatient->fio.'</span><br/>';
              $entry_body .= 'Субъект РФ: '.$tn->patient_terr_name.'<br/>';
              $entry_body .= 'МО: '.$tn->talonFields['VMP_STAGE1']['INST_NAME'].'<br/>';
              if($tn->itsMO()){
                $toUsers = getUsers('MU','',$tn->id_medinst);
              }else{
                $toUsers = getUsers('OUZ',$tn->ouz_terr);
              }
              $messageHelper->sendMessages(4, USER::$id, $toUsers, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
            }else{
              $entry_title = 'Обновлен 2 этап. Талон №'.$tn->uniq_number.'.';
              $prof_descr = ' (Профиль: '.str_replace('.','',isset($tn->talonFields['VMP_STAGE1']['PROFILE_CODE'])?$tn->talonFields['VMP_STAGE1']['PROFILE_CODE']:'').' - '.ucfirst(isset($tn->talonFields['VMP_STAGE1']['PROFILE_DESCR'])?$tn->talonFields['VMP_STAGE1']['PROFILE_DESCR']:'').')';
              $entry_body .= SandBox::$titleMU.' уведомляет Вас о том, что талон №'.$tn->uniq_number.(isset($prof_descr)?$prof_descr:'').' был обновлен на 2 этапе.<br/>';
              $entry_body .= '<br/>';
              $entry_body .= '<span class="link-act" onclick="goToEtap(3,'.$tn->id_talon.');">Перейти к '.($tn->itsMO()?'просмотру':'заполнению 3 этапа').'</span><br/><br/>';
              $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
              $entry_body .= $card->fieldsPatient->fio.'</span><br/>';
              $entry_body .= 'Субъект РФ: '.$tn->patient_terr_name.'<br/>';
              $entry_body .= 'МО: '.$tn->talonFields['VMP_STAGE1']['INST_NAME'].'<br/>';
              if($tn->itsMO()){
                $toUsers = getUsers('MU','',$tn->id_medinst);
              }else{
                $toUsers = getUsers('OUZ',$tn->ouz_terr);
              }
              $messageHelper->sendMessages(4, USER::$id, $toUsers, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
            }
          }
        }
      }
      
      /**
       * Если отказ у ОМС талона созданный медучреждением, то МО должно переводить сразу на 6 этап, т.к. ОУЗ уже не принимает участие!
       */
      if($tn->itsMO() && $statusOtkazDecis){
        $datAr['fio_resp_face'] = isset($_REQUEST['fio_resp_face'])?Service::stripText($_REQUEST['fio_resp_face'],100):' ';
        $datAr['position_name'] = isset($_REQUEST['position_name'])?$_REQUEST['position_name']:' ';
        $tn->saveStage(6, $rs, $statusOtkazDecis, $datAr);
      }
      
      if($statusOtkazDecis)
      {
            $prof_descr = ' (Профиль: '.str_replace('.','',$tn->talonFields['VMP_STAGE1']['PROFILE_CODE']).' - '.ucfirst($tn->talonFields['VMP_STAGE1']['PROFILE_DESCR']).')';
            $entry_title = 'Направленному Вами пациенту отказано в оказании ВМП. Номер талона: '.$tn->uniq_number;
            $entry_body .= 'Пациенту с номером талона '.$tn->uniq_number.$prof_descr.' отказано в оказании ВМП.<br/><br/>';
            $entry_body .= '<span class="link-act" onclick="goToEtap(6,'.$tn->id_talon.');">Перейти к '.($tn->itsMO()?'просмотру':'заполнению 6 Этапа').'</span><br/><br/>';
            $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
            $entry_body .= $card->fieldsPatient->fio.'</span><br/>';
            $entry_body .= 'Субъект РФ: '.$tn->patient_terr_name.'<br/>';
            $entry_body .= 'МО: '.$tn->talonFields['VMP_STAGE1']['INST_NAME'].'<br/>';
            if($tn->itsMO()){
              $toUsers = getUsers('MU','',$tn->id_medinst);
            }else{
              $toUsers = getUsers('OUZ',$tn->ouz_terr);
            }
            $messageHelper->sendMessages(4, USER::$id, $toUsers, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
      }
      
      // Сохранение даты изменения
      $sql = "update talon_napravlen set tms_mod =sysdate, max_etap =getmaxetap(id), last_user_id = ".user::$id.", list_wait =0 ".($tn->list_wait?', activated_by_mu=1, date_activated_by_mu=sysdate ':'')." where id = " . intval($tn->id_talon);
      $rs->execute($sql, array($tn->id_talon));
      if (Service::stripText($_REQUEST['s_comm']))
      {
        $s_comm = Service::stripText($_REQUEST['s_comm'],3000);
        $sql = "insert into vmp_stage_comments (id_talon_napr, id_etap, id_user, s_comm)
        values ( $tn->id_talon , 2, ".USER::$id." , '".$s_comm."' )";
        if($rs->execute($sql,array($tn->id_talon, USER::$id, "'$s_comm'"))){
            if(isset($tn->talonFields['VMP_STAGE2']['ID'])){
                // Уведомление МО
                $entry_title = 'К талону на 2 этапе добавлен комментарий. Номер талона: '.$tn->uniq_number;
                $entry_body = SandBox::$titleMU.' к талону добавил комментарий. Номер талона: '.$tn->uniq_number.'<br/><br/>';
                $entry_body .= '<span class="link-act" onclick="goToEtap(3,'.$tn->id_talon.');">Перейти к '.($tn->itsMO()?'просмотру':'заполнению 3 этапа').'</span><br/><br/>';
                $entry_body .= 'ФИО больного: <span class="link-act" onclick="goToCard('.$tn->id_patient.')">';
                $entry_body .= $card->fieldsPatient->fio.'</span>';
                $entry_body .= '<br/>Время отправки комментария:'.date("H:i:s");
                if($tn->itsMO()){
                  $toUsers = getUsers('MU','',$tn->id_medinst);
                }else{
                  $toUsers = getUsers('OUZ',$tn->ouz_terr);
                }
                $messageHelper->sendMessages(4, USER::$id, $toUsers, $entry_body, $entry_title, ['age_category' => $card->isChild() ? 1 : 2]);
            }
        }
      }
      $rs->commit();
    }
    $rs->close();
  }
  else
  {
    echo writeMessage('Не все обязательные поля заполнены!');
  }
}

